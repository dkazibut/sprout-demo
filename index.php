<?
	$root = 'http://www.sprout.local/'
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sprout Demo</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway:100,200,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?= $root; ?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= $root; ?>css/main.css">
	<script type="text/javascript" src="<?= $root; ?>js/mustache.js"></script>
	<script type="text/javascript" src="<?= $root; ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?= $root; ?>js/twitter-text.js"></script>
	<script type="text/javascript" src="<?= $root; ?>js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?= $root; ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= $root; ?>js/main.js"></script>
	<script id="template-tweet" type="x-tmpl-mustache">
		<ul id="tweet-list" class="list-unstyled">
		{{#tweets}}
			<li class="tweet{{#deleted}} mark-deleted{{/deleted}}{{#follow_up}} mark-follow-up{{/follow_up}}" data-tweet-id="{{id}}">
				<div class="media">
					<img src="{{profile_image_url}}" class="img-thumbnail pull-left">
					<div class="media-body">
						<p class="tweet-text">
							{{{text}}}
						</p>
						<p>
							 <a href="//twitter.com/{{screen_name}}" target="_blank">{{{name}}}</a> 
							 <br><span class="badge-created-at">{{created_at}}</span>
						</p>
					</div>
					
				</div>
				<ul class="tweet-actions list-unstyled">
					<li><i title="Mark this tweet for follow up" class="action-follow-up glyphicon glyphicon-star {{#follow_up}}selected{{/follow_up}}"></i></li>
					<li><i title="Delete this tweet" class="action-delete glyphicon glyphicon-trash {{#deleted}}deleted{{/deleted}}"></i></li>
				</ul>
			</li>
		{{/tweets}}
		</ul>
	</script>
</head>
<body class="list-all">
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<img id="logo" src="<?= $root;?>img/ss2-logo-gil.png" />
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a class="nav-all" href="#">All <span class="count-total badge"></span></a></li>
					<li><a class="nav-follow-up" href="#">Follow Up <span class="count-follow-up badge"></span></a> </li>
					<li><a class="nav-deleted" href="#">Deleted <span class="count-deleted badge"></span></a> </li>
				</ul>				
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="row">
			<div id="content" class="col-lg-6 col-lg-offset-3 col-xs-12 col-sm-12"> </div>
		</div>
	</div>
</body>
</html>
