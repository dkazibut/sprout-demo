$(document).ready(function(){
	//Load the JSON
	$.getJSON('js/tweets.json', function(data) {
		$('#content').html('<img src="../img/loader.gif">');
	})
	.success(function(data){
		var countDeleted = 0,
			countFollowUp = 0,
			countTotal = data.tweets.length;

		for (var i = 0; i < data.tweets.length; i++) {
			// Convert any spaces in the handle into &nbsp
			data.tweets[i].name = data.tweets[i].name.replace(/\s/g, '&nbsp;');
			
			// Clean up the date
			// data.tweets[i].created_at = prettyDate(data.tweets[i].created_at);
			data.tweets[i].created_at = moment(data.tweets[i].created_at).format('MMMM Do YYYY, h:mm a');
			
			// Convert links, @s and #s to <a>
			data.tweets[i].text = twttr.txt.autoLink(data.tweets[i].text);
		}

		// Put the number of tweets in a badge next to the nav item
		$('.count-deleted').html(countDeleted);
		$('.count-follow-up').html(countFollowUp);
		$('.count-total').html(countTotal);

		// Print the JSON to make sure that we got it okay
		// $('.col-md-6').html('<hr><pre>' + JSON.stringify(data.tweets, null, 4) + '</pre>');
		// console.log(data.tweets);

		// This is a dad-joke. Tweet + lister = Twister
		// More importantly, I don't want to pollute the global namespace with my functions
		Twister = {
			load: function() {
				var template,
					render;
				template = $('#template-tweet').html();
				Mustache.parse(template);
				render = Mustache.render(template, data);
				$('#content').html(render).hide();
				$('#content').fadeIn(600);

				// If any avatars 404 then replace the broken image with a blank
				$('img').error(function(){
					$(this).attr('src', 'img/avatar-blank.png');
				});
			},
			updateCountFollowUp: function(num) {
				num = num || 0;
				countFollowUp += num;
				$('.count-follow-up').html(countFollowUp);
				return countFollowUp;
			},
			updateCountDeleted: function(num) {
				num = num || 0;
				countDeleted += num;
				$('.count-deleted').html(countDeleted);
				return countDeleted;
			},
			updateCountTotal: function(num) {
				num = num || 0;
				countTotal += num;
				$('.count-total').html(countTotal);
				return countTotal;
			}
		};

		// This is the initial page load (load all tweets)
		Twister.load();

		// ############# NAVIGATION #############
		// All tweets button
		$('.nav-all').click(function(e){
			e.preventDefault();
			Twister.load();
			if (!$(this).parent().hasClass('active')) {
				$(this).parent().addClass('active');
				$('.nav-follow-up').parent().removeClass('active');
				$('.nav-deleted').parent().removeClass('active');
				$('body').removeClass().addClass('list-all');
			}
		});

		// Follow up tweets
		$('.nav-follow-up').click(function(e){
			e.preventDefault();
			if (!$(this).parent().hasClass('active')) {
				$(this).parent().addClass('active');
				$('.nav-all').parent().removeClass('active');
				$('.nav-deleted').parent().removeClass('active');
				$('body').removeClass().addClass('list-follow-up');
			}
			if (countFollowUp === 0) {
				$('#content').html('<h1>There are no tweets to display</h1>');
			} else {
				Twister.load();
			}
		});

		// Deleted tweets
		$('.nav-deleted').click(function(e){
			e.preventDefault();
			if (!$(this).parent().hasClass('active')) {
				$(this).parent().addClass('active');
				$('.nav-all').parent().removeClass('active');
				$('.nav-follow-up').parent().removeClass('active');
				$('body').removeClass().addClass('list-deleted');
			}
			if (countDeleted === 0) {
				$('#content').html('<h1>There are no tweets to display</h1>');
			} else {
				Twister.load();
			}
		});
		// ############# END NAVIGATION #############

		// ############# TWEET ACTIONS #############
		$(document.body).on('click', '.action-follow-up', function(){
			var index = $(this).parent().parent().parent().index();
			if ($(this).hasClass('selected')) {
				data.tweets[index].follow_up = false;
				Twister.updateCountFollowUp(-1);
			} else {
				data.tweets[index].follow_up = true;
				Twister.updateCountFollowUp(1);
			}
			$(this).toggleClass('selected');
		});

		$(document.body).on('click', '.action-delete', function(){
			var index = $(this).parent().parent().parent().index();
			if ($(this).hasClass('deleted')) {
				data.tweets[index].deleted = false;
				Twister.updateCountDeleted(-1);
				Twister.updateCountTotal(1);
				$(this).toggleClass('deleted');

			} else {
				if (window.confirm('Are you sure you want to delete this tweet?')) {
					data.tweets[index].deleted = true;
					Twister.updateCountDeleted(1);
					Twister.updateCountTotal(-1);
					$(this).parent().parent().parent().slideUp(200);
					$(this).toggleClass('deleted');
				}
			}
			
		});
		// ############# END TWEET ACTIONS #############
	})
	.fail(function(){
		var errorMessage = '<p class="center-text">No tweets found.</p>';
		$('#content').html(errorMessage);
	});
 });
